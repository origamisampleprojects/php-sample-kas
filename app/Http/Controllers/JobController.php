<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Models\Job;
use App\Http\Libraries\Common;
use App\Http\Models\ClientSite;
use App\Http\Models\JobTimesheet;
use App\Http\Models\JobWorkOrder;
use App\Http\Models\InventoryItem;
use App\Http\Libraries\JobLogging;
use App\Http\Models\JobPmiManifest;
use App\Http\Models\JobFuelManifest;
use App\Http\Models\JobDescriptWork;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use View;
use App;

class JobController extends Controller {

    use \ValidationRules;

    /**
    *  Display Job Listing 
    *  Can perform search & Can filter listing based on Job Status
    *
    *  @return View
    */
    public function index() {
        try {
            $search = Input::has('search') ? Input::get('search') : '';
            $filter = (Input::has('filter') && Input::get('filter') != 'all') ? Input::get('filter') : '';
            $currentPage = Input::has('page') ? Input::get('page') : 0;
            $perPage = Config::get('app.portal_items_per_page');

            $appendArr = array();
            if ($search) {
                $appendArr['search'] = $search;
            }

            if ($filter) {
                $appendArr['filter'] = $filter;
            }

            $jobListingObj = new Job();
            $jobListingObj = $jobListingObj->getPaginatedList($search, $filter, $currentPage, $perPage);

            if ($jobListingObj->count() == 0 && $jobListingObj->total() > 0) {
                return \Redirect::to($jobListingObj->appends($appendArr)->url($jobListingObj->lastPage()));
            }

            return \View::make('job.listing', compact('jobListingObj', 'appendArr', 'search', 'filter', 'currentPage'));
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput()->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     *  Selecting source for creating a new job
     *  Source can be Client with Client Site Or a Proposal
     *  
     *  @return View
     */
    public function selectJobFor() {
        
        $clientListObj = Client::listAll();

        $clientSiteListObj = NULL;
        $clientId = NULL;

        if ($clientListObj && $clientListObj->count()) {
            $clientId = $clientListObj->first()->id;
            $clientType = $clientListObj->first()->isOfType();
            $clientSiteListObj = ClientSite::listByClientID($clientListObj->first()->id);
        }

        $clientListObj = array(
            Config::get('constant.client.type.customer') => Client::listAll(Config::get('constant.client.type.customer')),
            Config::get('constant.client.type.prospect') => Client::listAll(Config::get('constant.client.type.prospect'))
        );

        $proposalListObj = Proposal::listAll();

        return \View::make('job.select', compact('clientId', 'clientType', 'clientListObj', 'clientSiteListObj', 'proposalListObj'));
    }

    /**
     * Store source for creating a new job
     *
     * @param Request $request
     * @return Redirect
     */
    public function storeJobCreationSource(Request $request) {
        try {

            if (!$request->has('source')) {
                return \Redirect::back()->with([
                    'serverError' => trans('messages.portal.error.job_create_session_error')
                ]);
            }

            $jobSourceArr = [];

            switch ($request->get('source')) {
                case 'client':
                    $jobSourceArr = [
                        'source' => $request->get('source'),
                        'client_id' => $request->get('client_id'),
                        'client_site_id' => $request->get('client_site_id')
                    ];
                    break;

                case 'proposal':
                    $jobSourceArr = [
                        'source' => $request->get('source'),
                        'proposal_id' => $request->get('proposal_id')
                    ];

                    break;
            }

            \Session::put('jobSource', $jobSourceArr);

            return \Redirect::route('portal.job.create');
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     *  Generate a view for creating new job
     *  
     *  @param  Request  $request
     *  @return View
     */
    public function create(Request $request) {
        
        $staffIDList = Staff::getStaffIDList();

        if (!\Session::has('jobSource.source')) {
            return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_session_error')
            ]);
        }

        $jobSource = \Session::get('jobSource.source');

        $proposalId = '';
        $proposalObj = NULL;
        $purchaseOrderNo = '';

        switch ($jobSource) {

            case 'client':
                $siteId = \Session::get('jobSource.client_site_id');
                break;

            case 'proposal':
                $proposalId = \Session::get('jobSource.proposal_id');
                $proposalObj = (new Proposal())->getByID($proposalId);

                if (!$proposalObj instanceof Proposal) {
                    return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_proposal')
                    ]);
                }

                $proposalObj->load('client');
                $proposalId = $proposalObj->id;
                $siteId = $proposalObj->client_site_id;
                break;
        }

        try {
            $clientSiteObj = new ClientSite();
            $clientSiteObj = $clientSiteObj->getByID($siteId);

            if (!$clientSiteObj instanceof ClientSite) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client_site')
                ]);
            }

            $clientSiteObj->load('client', 'state', 'country');

            if (!$clientSiteObj->client instanceof Client) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client')
                ]);
            }

            $jobCount = Job::getJobCountByClientId($clientSiteObj->client->id);

            $managerListObj = Staff::listAllByType(Config::get('constant.staff.type.manager'));

            $staffListObj = Staff::listAllByType(Config::get('constant.staff.type.technician'));

            $vehicleListObj = Vehicle::listAll();

            $inventoryItemListObj = InventoryItem::listAll();

            $jobNo = sprintf('%s%s%s', Common::firstLetters($clientSiteObj->client->name), Carbon::now()->format('Ymd'), sprintf('%02s', ($jobCount + 1)));

            $selectedItemsArr = [];

            if ($request->old('inventory') && is_array($request->old('inventory'))) {

                $selectedKeys = array_keys($request->old('inventory'));

                foreach ($inventoryItemListObj->toArray() as $invItem) {
                    if (in_array(array_get($invItem, 'id'), $selectedKeys)) {
                        $invItem['quantity'] = array_get($request->old('inventory'), array_get($invItem, 'id'));
                        $selectedItemsArr[] = $invItem;
                    }
                }
            }

            $selectedJobTypes = ($request->old('jobType') && is_array($request->old('jobType'))) ? $request->old('jobType') : [];

            $oldWorkOrderItemsArr = $request->old('workorder') ? $request->old('workorder') : [];
            $oldPmiManifestItemsArr = $request->old('pmimanifest') ? $request->old('pmimanifest') : [];
            $oldFuelManifestItemsArr = $request->old('fuelmanifest') ? $request->old('fuelmanifest') : [];
            $oldDescriptWorkItemsArr = $request->old('descriptwork') ? $request->old('descriptwork') : [];

            return \View::make('job.create', compact('clientSiteObj', 'staffListObj', 'managerListObj', 'vehicleListObj', 'inventoryItemListObj', 'proposalObj', 'selectedItemsArr', 'proposalId', 'jobNo', 'purchaseOrderNo', 'selectedJobTypes', 'oldWorkOrderItemsArr', 'oldPmiManifestItemsArr', 'oldFuelManifestItemsArr', 'oldDescriptWorkItemsArr'));
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     *  Validate and create new job from posted data
     *
     *  @param  Request  $request
     *  @return Redirect
     */
    public function store(Request $request) {
         
        if (!\Session::has('jobSource.source')) {
            return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_session_error')
            ]);
        }

        $validationRules = self::getJobValidationRules();

        $validator = \Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return \Redirect::back()->withInput($request->all())
                            ->withErrors($validator);
        }

        try {
            $scheduleDate = Carbon::createFromFormat('m/d/Y', array_get($request->all(), 'scheduleDate'));
            $scheduleDate = $scheduleDate->startOfDay();
            if ($scheduleDate->lt(Carbon::yesterday())) {
                $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_older_than_today'));
                return \Redirect::back()->withInput($request->all())
                                ->withErrors($validator);
            }
        } catch (Exception $exp) {
            $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_invalid_format'));
            return \Redirect::back()->withInput($request->all())
                            ->withErrors($validator);
        }

        $jobSource = \Session::get('jobSource.source');

        $proposalId = '';
        $proposalObj = NULL;

        switch ($jobSource) {

            case 'client':
                $siteId = \Session::get('jobSource.client_site_id');
                break;

            case 'proposal':
                $proposalId = \Session::get('jobSource.proposal_id');
                $proposalObj = (new Proposal())->getByID($proposalId);

                if (!$proposalObj instanceof Proposal) {
                    return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_proposal')
                    ]);
                }

                $proposalId = $proposalObj->id;
                $siteId = $proposalObj->client_site_id;
                break;
        }

        try {
            $clientSiteObj = new ClientSite();
            $clientSiteObj = $clientSiteObj->getByID($siteId);

            if (!$clientSiteObj instanceof ClientSite) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client_site')
                ]);
            }

            $clientSiteObj->load('client');

            if (!$clientSiteObj->client instanceof Client) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client')
                ]);
            }

            \DB::beginTransaction();

            $jobCount = Job::getJobCountByClientId($clientSiteObj->client->id);
            $jobNo = sprintf('%s%s%s', Common::firstLetters($clientSiteObj->client->name), Carbon::now()->format('Ymd'), sprintf('%02s', ($jobCount + 1)));

            $jobFilteredData = $request->only('title', 'priority', 'purchaseOrderNo', 'scheduleDate', 'description', 'specialInstruction', 'signoffRequired');
            
            $jobFilteredData = Common::standardizeColumnValues($jobFilteredData, ['signoffRequired']);

            $workOrderFilteredData = $request->has('workorder') ? $request->get('workorder') : [];
            $pmiManifestFilteredData = $request->has('pmimanifest') ? $request->get('pmimanifest') : [];
            $fuelManifestFilteredData = $request->has('fuelmanifest') ? $request->get('fuelmanifest') : [];
            $descriptWorkFilteredData = $request->has('descriptwork') ? $request->get('descriptwork') : [];
            $jobResourceFilteredData = $request->only('manager', 'staff', 'vehicle', 'inventory');

            $jobObj = new Job();
            $jobObj->setAssociationData($clientSiteObj->client->id, $clientSiteObj->id, $proposalId, $scheduleDate, $jobNo)->fillnSave($jobFilteredData);

            $jobObj->setJobType($request->get('jobType'))
                    ->createOrUpdateWorkOrders($workOrderFilteredData)
                    ->createOrUpdatePMIManifests($pmiManifestFilteredData)
                    ->createOrUpdateFuelManifests($fuelManifestFilteredData)
                    ->createOrUpdateDescriptWorkForms($descriptWorkFilteredData);

            $jobObj->allocatedResources($jobResourceFilteredData);

            $jobObj->addNewJobcode();

            if ($proposalObj && !$proposalObj->isAccepted()) {
                $proposalObj->updateStatus(Config::get('constant.status.accepted'));
            }


            if($request->hasFile('attachments')){    
                $attachedIdArr = [];
                
                foreach ($request->file('attachments') as $key => $jobAttachment){
                    
                    $attachedId =  $jobObj->addAttachment($jobAttachment, 'job_file_path'); 

                    if( ! $attachedId ){
                        if(count($attachedIdArr) > 0 )
                            $jobObj->deleteAttachments($attachedIdArr);

                        \DB::rollback();
                        
                        return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.ajax.invalid_attached_document.message')
                        ]);
                    }
                   
                   $attachedIdArr[] = $attachedId;
                }

            }    

            \DB::commit();
            
            return \Redirect::route('portal.job.listing')->with([
                        'success' => trans('messages.portal.success.job_created_successfully', array('jobNo' => $jobObj->jobNo))
            ]);
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);    
            return \Redirect::back()->withInput($request->all())
                    ->with([
                    'serverError' => trans('messages.portal.error.server_error') 
                    ]);
        }
    }


    /**
     *   Generate View for editing a given Job
     *
     *  @param  Request  $request 
     *  @param  number  $jobId
     *  @return View
     */
    public function edit(Request $request, $jobId) {
        
        $jobObj = new Job();
        $jobObj = $jobObj->getByID($jobId);

        if (!$jobObj instanceof Job) {
            return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
            ]);
        }

        $jobObj->load('client', 'clientSite', 'proposal', 'document', 'note', 'note.staff');

        if (!$jobObj->client instanceof Client) {
            return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client')
            ]);
        }

        if (!$jobObj->clientSite instanceof ClientSite) {
            return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client_site')
            ]);
        }

        if ($jobObj->proposal_id && !$jobObj->proposal instanceof Proposal) {
            return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_proposal')
            ]);
        }

        if ($jobObj->isCancelled() || $jobObj->isCompleted()) {
            return \Redirect::route('portal.job.view', $jobObj->id)->with([
                    'serverError' => trans('messages.portal.error.job_edit_not_allowed')
            ]);
        }

        try {

            $jobResources = $jobObj->getResources();
            $jobManagerResources = $request->old('manager') ? $request->old('manager') : Common::mapArrayColumn(array_get($jobResources, 'manager'));
            $jobStaffResources = $request->old('staff') ? $request->old('staff') : Common::mapArrayColumn(array_get($jobResources, 'staff'));
             
            $jobVehicleResources = $request->old('vehicle') ? $request->old('vehicle') : Common::mapArrayColumn(array_get($jobResources, 'vehicle'));

            $staffListObj = Staff::listAllByType(Config::get('constant.staff.type.technician'));
            $managerListObj = Staff::listAllByType(Config::get('constant.staff.type.manager'));

            $workingStaffList = Staff::listAllWorkingByType([
                        Config::get('constant.staff.type.technician'),
                        Config::get('constant.staff.type.manager')], $jobObj->id);


            if ($workingStaffList) {
                $workingStaffList = Common::mapArrayColumn($workingStaffList->toArray(), 'id');
            }

            $jobTimesheets = JobTimesheet::getJobRelatedTimesheets($jobId);
            
            $vehicleListObj = Vehicle::listAll();
            $inventoryItemListObj = InventoryItem::listAll();

            // Creating Array of Selected inventory Items
            $selectedItemsArr = [];

            if ($request->old('inventory') && is_array($request->old('inventory'))) {
                $selectedKeys = array_keys($request->old('inventory'));
                foreach ($inventoryItemListObj->toArray() as $invItem) {
                    if (in_array(array_get($invItem, 'id'), $selectedKeys)) {
                        $invItem['quantity'] = array_get($request->old('inventory'), array_get($invItem, 'id'));
                        $selectedItemsArr[] = $invItem;
                    }
                }
            } else {
                $selectedItemsArr = Common::mapInventoryItemColumn(array_get($jobResources, 'inventory'));
            }

            $jobObj->load('type', 'workorder', 'pmimanifest', 'fuelmanifest', 'descriptwork', 'descriptwork.timeentry', 'descriptwork.material', 'descriptwork.subcontractor', 'document');

            $selectedJobTypes = ($request->old('jobType') && is_array($request->old('jobType'))) ? $request->old('jobType') : Common::mapArrayColumn($jobObj->type->toArray(), 'type');

            $selectedWorkOrderItemsArr = $request->old('workorder') ? $request->old('workorder') : $jobObj->workorder->toArray();
            $selectedPmiManifestItemsArr = $request->old('pmimanifest') ? $request->old('pmimanifest') : $jobObj->pmimanifest->toArray();
            $selectedFuelManifestItemsArr = $request->old('fuelmanifest') ? $request->old('fuelmanifest') : $jobObj->fuelmanifest->toArray();
            $selectedDescriptWorkItemsArr = $request->old('descriptwork') ? $request->old('descriptwork') : $jobObj->descriptwork->toArray();

            return \View::make('job.edit', compact('managerListObj', 'staffListObj', 'jobManagerResources', 'jobStaffResources', 'workingStaffList', 'vehicleListObj', 'jobVehicleResources', 'inventoryItemListObj', 'jobObj', 'selectedItemsArr', 'selectedJobTypes', 'selectedWorkOrderItemsArr', 'selectedPmiManifestItemsArr', 'selectedFuelManifestItemsArr', 'selectedDescriptWorkItemsArr', 'jobTimesheets'));
        } catch (\Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);

            return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     *  Validate & update selected Job Resource
     *
     *  @param  Request $request
     *  @param  string  $jobId
     *  @return Reditect
     */
    public function update(Request $request, $jobId) {

          
        $validationRules = self::getJobUpdateValidationRules();

        try {
            $jobObj = new Job();
            

            $jobObj = $jobObj->getByID($jobId);

            if (!$jobObj instanceof Job) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
                ]);
            }
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }

        if ($jobObj->isInprogress()) {
            $validationRules = array_except($validationRules, ['title', 'scheduleDate']);
        }

        $validator = \Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return \Redirect::back()->withInput($request->all())
                            ->withErrors($validator);
        }

        try {
            if (array_get($request->all(), 'scheduleDate') && $jobObj->isPending()) {
                $scheduleDate = Carbon::createFromFormat('m/d/Y', array_get($request->all(), 'scheduleDate'));
                $scheduleDate = $scheduleDate->startOfDay();
                if ($scheduleDate->lt(Carbon::yesterday())) {
                    $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_older_than_today'));
                    return \Redirect::back()->withInput($request->all())
                                    ->withErrors($validator);
                }
            }
        } catch (Exception $exp) {
            $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_invalid_format'));
            return \Redirect::back()->withInput($request->all())
                            ->withErrors($validator);
        }

        try {

            if (!$jobObj instanceof Job) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
                ]);
            }

            $jobObj->load('client', 'clientSite', 'proposal');

            if (!$jobObj->client instanceof Client) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client')
                ]);
            }

            if (!$jobObj->clientSite instanceof ClientSite) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client_site')
                ]);
            }

            if ($jobObj->proposal_id && !$jobObj->proposal instanceof Proposal) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_proposal')
                ]);
            }

            if ($jobObj->isCancelled() || $jobObj->isCompleted())
                return \Redirect::route('portal.job.view', $jobObj->id)->with([
                    'serverError' => trans('messages.portal.error.job_edit_not_allowed')
                ]);

            \DB::beginTransaction();

            $selectedJobTypes = $request->has('jobType') ? $request->get('jobType') : [];
            
            $workOrderFilteredData    = $request->has('workorder')    ? $request->get('workorder') : [];
            $pmiManifestFilteredData  = $request->has('pmimanifest')  ? $request->get('pmimanifest') : [];
            $fuelManifestFilteredData = $request->has('fuelmanifest') ? $request->get('fuelmanifest') : [];
            $descriptWorkFilteredData = $request->has('descriptwork') ? $request->get('descriptwork') : [];

            if ($jobObj->isPending()) {

                $jobFilteredData = $request->only('title', 'priority', 'purchaseOrderNo', 'scheduleDate', 'description', 'specialInstruction', 'signoffRequired');
                $jobFilteredData['scheduled_at'] = $scheduleDate;
                $jobResourceFilteredData = $request->only('manager', 'staff', 'vehicle', 'inventory');

            } else if ($jobObj->isInprogress()) {

                $jobFilteredData = $request->only('priority', 'purchaseOrderNo', 'description', 'specialInstruction' , 'signoffRequired');
                $jobResourceFilteredData = $request->only('manager', 'staff');
            }

            $workingTechincianList = Staff::listAllWorkingByType([
                        Config::get('constant.staff.type.technician')], $jobObj->id);

            $selectedTechicians = array_get($jobResourceFilteredData , 'staff');

            if( count($workingTechincianList) > 0 ){
                // Currently Technicians are working on the Job
                $workingTechincianNameList = [];
                foreach($workingTechincianList as $technicianObj){
                    if(! in_array( $technicianObj->id , $selectedTechicians) ){
                        $workingTechincianNameList[] =  Common::getFullName($technicianObj);    
                    }
                }

                if( count($workingTechincianNameList) > 0 ){
                    $implodedValues = implode(', ', $workingTechincianNameList);
                    return \Redirect::back()->withInput($request->all())->with([
                    'serverError' => trans('messages.portal.error.job_cannot_remove_working_technician', ['staffNames' =>  $implodedValues ])
                        ]);
                }
            }

            $timesheetFilteredData = $request->has('timesheet') ? $request->get('timesheet') : [];

            $jobObj->fillnSave($jobFilteredData);

            $deletedWorkOrder    = $request->has('deletedWorkOrder') ? $request->get('deletedWorkOrder') : [];
            $deletedPmiManifest  = $request->has('deletedPmiManifest') ? $request->get('deletedPmiManifest') : [];
            $deletedFuelManifest = $request->has('deletedFuelManifest') ? $request->get('deletedFuelManifest') : [];
            $deletedDescriptWork = $request->has('deletedDescriptWork') ? $request->get('deletedDescriptWork') : [];
            $deletedAttachment   = $request->has('deletedAttachment') ? $request->get('deletedAttachment') : [];

             
            $jobObj->deleteWorkOrders($deletedWorkOrder)
                    ->deletePMIManifests($deletedPmiManifest)
                    ->deleteFuelManifests($deletedFuelManifest)
                    ->deleteDescriptWorkForms($deletedDescriptWork);
            
            $delAttachedStatus = $this->deleteAttachments($deletedAttachment);

            $jobObj->setJobType($selectedJobTypes)
                    ->updateTimesheets($timesheetFilteredData)
                    ->createOrUpdateWorkOrders($workOrderFilteredData)
                    ->createOrUpdatePMIManifests($pmiManifestFilteredData)
                    ->createOrUpdateFuelManifests($fuelManifestFilteredData)
                    ->createOrUpdateDescriptWorkForms($descriptWorkFilteredData);



            if ($jobResourceFilteredData && ( $jobObj->isPending() || $jobObj->isInprogress() )) {
                $jobObj->allocatedResources($jobResourceFilteredData);
            }

            if($request->hasFile('attachments')){

                $attachedIdArr = [];
                
                foreach ($request->file('attachments') as $key => $jobAttachment){
                    
                    $attachedId =  $jobObj->addAttachment($jobAttachment, 'job_file_path'); 

                    if( ! $attachedId ){
                        if(count($attachedIdArr) > 0 )
                            $jobObj->deleteAttachments($attachedIdArr);

                        \DB::rollback();
                        
                        return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.ajax.invalid_attached_document.message')
                        ]);
                   }
                   
                   $attachedIdArr[] = $attachedId;
                }

            } 

            \DB::commit();
        

            return \Redirect::route('portal.job.listing')->with([
                        'success' => trans('messages.portal.success.job_updated_successfully', array('jobNo' => $jobObj->jobNo))
            ]);
        } catch (\Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

   /**  
    *  Delete Attachments by ID given by an array
    *
    *  @param  array $attachmentIdArr
    *  @return boolean
    */

    public function deleteAttachments($attachmentIdArr)
    {
        try{
            if(!empty($attachmentIdArr)){
                $response = null;
                foreach ( $attachmentIdArr as $attachmentId )
                {
                    $documentObj = (new Document())->getByID($attachmentId);
                    if(! $documentObj instanceof Document){
                        $response = 'error';
                    }
                    if($documentObj instanceof Document){
                        $docPath = $documentObj->path;
                        if($documentObj->delete()){
                            unlink($docPath);
                        }
                    }
                }

                if(!empty($response)){
                    return $response;
                } 
                else{
                    return true;
                }
            }
             
        }
        catch(\Exception $exp){
            self::logError($exp, __CLASS__, __METHOD__);
            return false;
        }   
        
    }   

    /**
    *  Create View of Preview Screen for Job
    *
    *  @param  Request $request
    *  @param  number  $jobId
    *  @return  View 
    */
    public function preview(Request $request, $jobId) {
        try {
            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);

            if (!$jobObj instanceof Job) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
                ]);
            }

            $jobObj->load('client', 'clientSite', 'proposal', 'document', 'note', 'note.staff', 'workorder', 'pmimanifest', 'fuelmanifest', 'descriptwork', 'descriptwork.timeentry', 'descriptwork.material', 'descriptwork.subcontractor', 'signoff', 'signoff.staff');

            if (!$jobObj->client instanceof Client) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client')
                ]);
            }

            if (!$jobObj->clientSite instanceof ClientSite) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client_site')
                ]);
            }

            if ($jobObj->proposal_id && !$jobObj->proposal instanceof Proposal) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_proposal')
                ]);
            }

            $jobResources = $jobObj->getResources();
            $jobStaffResource = NULL;

            if (array_get($jobResources, 'staff')) {

                $jobStaffResource = JobTimesheet::getJobRelatedTimesheets($jobId, array_get($jobResources, 'staff'));
                $jobResources['staff'] = $jobStaffResource;
            }
            
            $this->calculateJobTime($jobResources);

            
            $selectedWorkOrderItemsArr = $jobObj->workorder->toArray()?: [];
            $selectedPmiManifestItemsArr = $jobObj->pmimanifest->toArray()?: [];
            $selectedFuelManifestItemsArr = $jobObj->fuelmanifest->toArray()?: [];
            $selectedDescriptWorkItemsArr = $jobObj->descriptwork->toArray()?: [];

            return \View::make('job.preview', compact('jobObj', 'jobResources', 'jobNotes', 'selectedWorkOrderItemsArr', 'selectedPmiManifestItemsArr', 'selectedFuelManifestItemsArr', 'selectedDescriptWorkItemsArr'));
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                            ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
    *  Change Status of given Job to Cancelled
    *
    *  @param  Request $request
    *  @param  number  $jobId
    *  @return  json  Response 
    */
    public function cancel(Request $request, $jobId) {
        try {
            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);

            if (!$request->has('note')) {
                return \Response::json(array(
                            'code' => trans('messages.portal.error.ajax.job_cancellation_note_not_present.code'),
                            'body' => trans('messages.portal.error.ajax.job_cancellation_note_not_present.message')
                                ), 412);
            }

            if (!$jobObj instanceof Job) {
                return \Response::json(array(
                            'code' => trans('messages.portal.error.ajax.job_not_found.code'),
                            'body' => trans('messages.portal.error.ajax.job_not_found.message')
                                ), 404);
            }

            if ($jobObj->status != Config::get('constant.status.pending')) {

                return \Response::json(array(
                            'code' => trans('messages.portal.error.ajax.job_cancelled_only_for_pending_job.code'),
                            'body' => trans('messages.portal.error.ajax.job_cancelled_only_for_pending_job.message')
                                ), 404);
            }

            \DB::beginTransaction();

            $jobObj = $jobObj->updateStatus(Config::get('constant.status.cancelled'));
            $jobObj->addNote(Auth::user()->id, config('constant.status.cancelled'), strip_tags($request->get('note')));
            \DB::commit();
            return \Response::json(array(
                        'code' => trans('messages.portal.success.ajax.job_cancelled_successfully.code'),
                        'body' => trans('messages.portal.success.ajax.job_cancelled_successfully.message')
                            ), 200);
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Response::json(array(
                        'code' => 200,
                        'body' => trans('messages.portal.error.server_error')
                            ), 500);
        }
    }

    /**
    *  Close Job after Job completion
    *
    *  @param  Request $request
    *  @param  number  $jobId
    *  @return  Redirect
    */
    public function close(Request $request, $jobId){
        try {

            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);

            if (!$jobObj instanceof Job) {
                return \Redirect::to(route('portal.job.listing') )->with(['serverError' => trans('messages.portal.error.job_not_found')]);
            }

            if ( $jobObj->isCompleted() ) {
                return \Redirect::to( route('portal.job.view', $jobObj->id) )->with(['serverError' => trans('messages.portal.error.cannot_close_not_closed_job')]);
            }

            if ( ! $jobObj->isInprogress() ) {
                return \Redirect::back()->with(['serverError' => trans('messages.portal.error.cannot_close_not_inprogress_job')]);
            }

            
            $openedTaskListObj = JobTimesheet::getOpenedTaskByJobId($jobObj->id);
            
            if( $openedTaskListObj ){
            
                $workingStaffName = [];
                foreach ($openedTaskListObj as $key => $jobTimesheetObj) {
                    $fullName = Common::getFullName($jobTimesheetObj->staff);
                    if( ! in_array($fullName, $workingStaffName) ){
                        $workingStaffName[] = $fullName;    
                    }
                }

                if( count($workingStaffName) > 0 ){
                    $implodedValues = implode( ', ' , $workingStaffName );
                    return \Redirect::back()->with(['serverError' => trans('messages.portal.error.job_cannot_close_technician_working', [ 'staffNames' => $implodedValues] ) ] );
                }
            }

            \DB::beginTransaction();

            $jobObj = $jobObj->updateStatus(Config::get('constant.status.completed'));
            $jobCloseNotes = sprintf("Job closed by %s", Common::getFullName( Auth::user() ) );
            $jobObj->addNote(Auth::user()->id, config('constant.status.completed'), $jobCloseNotes );

            \DB::commit();

            return \Redirect::to(route('portal.job.view', $jobObj->id))->with(['success' => trans('messages.portal.success.job_marked_completed_successfully')]);

        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::to(route('portal.job.view', $jobObj->id))
                ->with([
                    'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Calculate Job Time using Staff Timesheets
     * This function calculate and set job time i.e.
     * total, billable , nonbillable .
     * It also add totalTime taken by each staff using the timesheet
     *
     * @param array &$jobResourceArr
     */
    private function calculateJobTime(&$jobResourceArr) {
        $jobResourceArr['time'] = $jobTime = array(
            'total' => 0,
            'billable' => 0,
            'nonbillable' => 0
        );

        if (array_get($jobResourceArr, 'staff')) {

            foreach ($jobResourceArr['staff'] as $sKey => $staff) {
                $jobResourceArr['staff'][$sKey]['totalTime'] = 0;

                if (array_get($staff, 'timesheet')) {

                    foreach ($staff['timesheet'] as $sTKey => $timesheet) {

                        $jobResourceArr['staff'][$sKey]['totalTime'] += (int) array_get($timesheet, 'duration');

                        $jobTime['total'] += (int) array_get($timesheet, 'duration');

                        if (array_get($timesheet, 'billable') == Config::get('constant.option.true'))
                            $jobTime['billable'] += (int) array_get($timesheet, 'duration');
                        else
                            $jobTime['nonbillable'] += (int) array_get($timesheet, 'duration');
                    }
                }
            }
            $jobResourceArr['time'] = $jobTime;
        }
    }

    /**
    *  Get Job resource by jobId   
    *
    * @param  Request  $request
    * @param  string  $resourceType
    * @param  number  $jobId
    * @return Response
    **/
    public function getAssignedResources(Request $request, $jobId, $resourceType){
        try{
            
            $jobObj = (new Job())->getByID($jobId); 
            if( ! $jobObj instanceof Job){
                $this->error = "job_not_found";
                return $this->sendResponse('portal');
            }

            if ( ! in_array( $resourceType , Config::get('constant.resource.type') ) ){
                $this->error = "invalid_job_resource_type";
                return $this->sendResponse('portal');   
            }
            
            $assignedResources = $jobObj->getResources([$resourceType]);
            $resourceIdArr = [];
            
            if($assignedResources[$resourceType]){
                $resourceIdArr = Common::filterSingleArrayColumn($assignedResources[$resourceType], 'id');
            }

            return \Response::json(array(
                'responseCode' => 200,
                'response' => ['resource' => $resourceIdArr]
                )
            , 200);

        } catch(\Exception $exp){
            self::logError($exp, __CLASS__, __METHOD__);
            $this->error = "server_error";
            return $this->sendResponse('portal');
        }
    }

    /**
     *  Generate Job Form Signature as Png Image
     *
     *  @param string $jobFormType;
     *  @param number $jobFormId;
     *  @param string $signatureKey;
     *  @return image PNG
     */
    public function generateJobFormSignature($jobFormType, $jobFormId, $signatureKey){
        

        $imageContent = base64_decode("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEUAAACnej3aAAAAAXRSTlMAQObYZgAAAApJREFUCNdjYAAAAAIAAeIhvDMAAAAASUVORK5CYII=");

        if( in_array( $jobFormType, Config::get('constant.job.form.type') ) && $jobFormId && array_key_exists( $signatureKey, Config::get('constant.job.form.signature') ) ){
            
            try{
                $jobFormModel = array_get(Config::get('constant.job.form.model'), $jobFormType);
                $jobFormObj = (new $jobFormModel())->getByID( $jobFormId );
                
                if($jobFormObj instanceof $jobFormModel){
                    
                    $jobFormArr = $jobFormObj->toArray();
                    $signatureImage = array_get($jobFormArr, array_get(config::get('constant.job.form.signature') , $signatureKey) );
                    $imageContent = $signatureImage ? base64_decode( preg_replace('/[\r\n]/','', preg_replace('~\x{00a0}~', '', $signatureImage)) ) : $imageContent;
                }

            } catch (\Exception $exp){
                self::captureData(['jobFormType' => 'Doc Type', 'jobFormId' => $jobFormId, 'signatureKey' => $signatureKey], __CLASS__, __METHOD__);
                self::logError($exp, __CLASS__, __METHOD__);
            } 
        }

        $response = \Response::make($imageContent, 200);
        
        $response->header('Content-Type', 'image/png');
        return $response;
    }

    /**
     *  Generate PDF of Job Workorder By workOrderId
     *
     *  @param number $workOrderId;
     *  @return App\Http\Models\JobWorkOrder | NULL
     */
    public function generateDownloadWorkOrderPDF($workOrderId) {
        try{
            $workOrderObj = (new JobWorkOrder())->getByID($workOrderId);
            
            if ( ! $workOrderObj instanceof JobWorkOrder) {
                return redirect()->back()->with(['serverError' => trans('message.portal.error.invalid_workorder_selected')]);
            }

            $workOrderObj->load('job' , 'job.client' , 'job.clientSite');

            $downloadFileName = sprintf( '%s-%s.pdf' , $workOrderObj->title , $workOrderObj->job->jobNo);

            $workOrderObj->notes = Common::getWordWrappedArray($workOrderObj->notes, 135);
            $workOrderObj->material = Common::getWordWrappedArray($workOrderObj->material, 30);
            $workOrderObj->equipment = Common::getWordWrappedArray($workOrderObj->equipment, 43);

            $workOrderView = View::make('job/templates/pdf/workorder', [ "workOrderObj" => $workOrderObj]);
            $workOrderHtml = $workOrderView->render();
            $pdfObj = App::make('dompdf.wrapper');
            $pdfObj->loadHTML($workOrderHtml)->setPaper('a4', 'portrait');

            return $pdfObj->download( $downloadFileName, array("Attachment" => false));
        }
        catch(\Exception $exp){
            self::logError($exp, __CLASS__, __METHOD__);
            return redirect()->back()->with(['serverError' => trans('messages.portal.error.workorder_not_generated')]);
        }

    }

    
    /**
    * Clean Out Encoded Image Data indicated in columns
    *  
    * @param Object $jobFormObj  Job Form Object by reference (JobWorkOrder|JobPMIManifest|JobFuelManifest|JobDescriptWork)
    * @param Object $columns  Array of model attributes which contains encoded Image Data
    * 
    */
    private function cleanOutImageDataURI(&$jobFormObj, $columns = []){
        
        if( is_array($columns) && count($columns) > 0 ) {
            foreach ( $columns as $key => $colName ) {
                if( $jobFormObj->__isset($colName) ){
                    $jobFormObj->$colName = Common::cleanoutEncodedPNGDataURI($jobFormObj->$colName);
                }
            }
            
        }
    }

}
