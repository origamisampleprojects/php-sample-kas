<?php
namespace App\Http\Models;

use Carbon\Carbon;
use App\Http\Models\Jobcode;
use App\Http\Models\JobNote;
use App\Http\Models\JobType;
use App\Http\Models\Document;
use App\Http\Models\JobResource;
use App\Http\Models\JobWorkOrder;
use App\Http\Models\JobPmiManifest;
use Illuminate\Support\Facades\Config;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\JobDescriptWorkTimeEntry;

class Job extends Model
{
    use \DataMapper, \Logger, \JobFormOperation, \JobTimesheetOperation , \AttachmentOperation;

    protected $table = "job";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jobNo',
        'purchaseOrderNo',
        'client_id',
        'client_site_id',
        'proposal_id',
        'title',
        'priority',
        'type',
        'description',
        'specialInstruction',
        'followup_job_id',
        'scheduled_at',
        'signoffRequired',
        'status'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

     /**
     * Get Fillable Keys That Need Standardization
     * Standardization involves make value True OR False
     *
     * @return array
     */
    public function getNeedStandarizeKeys(){
        return [
            'signoffRequired'
        ];
    }

    /**
    *  Appending Job Response to Include SignOff Count Key 
    */
    public function includeSignoffCount(){
        $this->appends += ['signoffCount'];
        return $this;
    }

    /**
     * Belongs To Relation with Client
     */
    public function client()
    {
        return $this->belongsTo('App\Http\Models\Client', 'client_id', 'id');
    }

    /**
     * Belongs To Relation with Client Site
     */
    public function clientSite()
    {
        return $this->belongsTo('App\Http\Models\ClientSite', 'client_site_id', 'id');
    }

    /**
     * Belongs To Relation with Proposal
     */
    public function proposal()
    {
        return $this->belongsTo('App\Http\Models\Proposal', 'proposal_id', 'id');
    }

    /**
     * Has Many Relation with JobNotes
     */
    public function note()
    {
        return $this->hasMany('App\Http\Models\JobNote');
    }

    /**
     * Has Many Relation with JobWorkOrder
     */
    public function workorder()
    {
        return $this->hasMany('App\Http\Models\JobWorkOrder');
    }

    /**
     * Has Many Relation with JobPmiManifest
     */
    public function pmimanifest()
    {
        return $this->hasMany('App\Http\Models\JobPmiManifest');
    }

    /**
     * Has Many Relation with JobFuelManifest
     */
    public function fuelmanifest()
    {
        return $this->hasMany('App\Http\Models\JobFuelManifest');
    }

    /**
     * Has Many Relation with JobWorkOrder
     */
    public function descriptwork()
    {
        return $this->hasMany('App\Http\Models\JobDescriptWork');
    }

    /**
     * Has Many Relation with JobType
     */
    public function timesheet()
    {
        return $this->hasMany('App\Http\Models\JobTimesheet');
    }

    /**
     * Has Many Relation with JobType
     */
    public function type()
    {
        return $this->hasMany('App\Http\Models\JobType');
    }

    /**
    * Has Many Relation with JobSignoff
    */
    public function signoff()
    {
        return $this->hasMany('App\Http\Models\JobSignoff');
    }

    /**
    *   Relation to count signoff for current job
    */
    public function signoffCountRelation()
    {
        return $this->hasOne('App\Http\Models\JobSignoff')
            ->selectRaw('job_id, count(*) as count')->groupBy('job_id');       
    }

    /**
     * One-to-Many Polymorphic Relation with Job Resources
     */
    public function resources()
    {
        return $this->leftJoin('job_resource', 'job_resource.job_id', '=', 'job.id')
            ->where('job_resource.job_id', '=', $this->id)
            ->select('job_resource.*')
            ->get();
    }

    /**
     * One-to-Many Polymorphic Relation with Document
     */
    public function document()
    {
        return $this->morphMany('App\Http\Models\Document', 'belongTo');
    }

    /**
    *  Custom Attribute signoffCount
    */
    public function getSignoffCountAttribute(){
        
        return ( ( isset($this->signoffCountRelation) && isset($this->signoffCountRelation->count))? $this->signoffCountRelation->count: 0) ;
    }


    /**
    *  Get Staff Related to Job
    */
    public function staff()
    {
        return $this->leftJoin('job_resource', 'job_resource.job_id', '=', 'job.id')
            ->where('job_resource.job_id', '=', $this->id)
            ->where('job_resource.resource_type', '=', Config::get('constant.resource.model.staff'))
            ->select('job_resource.*')
            ->get();
    }

    public function getScheduleDateAttribute()
    {
        return $this->attributes['scheduled_at'] ? Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['scheduled_at'])->format('m/d/Y') : NULL;
    }
    
    /**
    *  Determines if the status of Job is pending
    */
    public function isPending()
    {
        return ($this->status == Config::Get('constant.status.pending') ? true : false);
    }

    /**
    *  Determines if the status of Job is pending
    */
    public function isCancelled()
    {
        return ($this->status == Config::Get('constant.status.cancelled') ? true : false);
    }

    /**
    *  Determines if the status of Job is inprogress
    */
    public function isInprogress()
    {
        return ($this->status == Config::Get('constant.status.inprogress') ? true : false);
    }

    /**
    *  Determines if the status of Job is compeleted
    */
    public function isCompleted()
    {
        return ($this->status == Config::Get('constant.status.completed') ? true : false);
    }

    /**
     * Get Job By ID
     *
     * @param number $jobId;            
     * @return App\Http\Models\Job | NULL
     */
    public function getByID($jobId)
    {
        return $this->where('id', '=', $jobId)->first();
    }

    /**
     * Get Job Detail By Job ID
     *
     * @param number $jobId;            
     * @return App\Http\Models\Job | NULL
     */
    public function getJobDetail($jobId)
    {
        
        return $this->with(['type', 'workorder', 'pmimanifest', 'fuelmanifest', 'descriptwork', 'descriptwork.timeentry','descriptwork.material','descriptwork.subcontractor', 'clientSite','clientSite.state','clientSite.country'])
            ->where('job.id', '=', $jobId)
            ->whereIn('job.status', [
                \Config::get('constant.status.pending'),
                \Config::get('constant.status.inprogress'),
                \Config::get('constant.status.completed')
            ])->first();
    }

    /**
     * Set Value for Job Associated
     *
     * @param number $clientId            
     * @param number $clientSiteId            
     * @param number $proposalId            
     * @param Date $scheduleDate            
     * @return this
     */
    public function setAssociationData($clientId, $clientSiteId, $proposalId, $scheduleDate, $jobNo)
    {
        $this->client_id = $clientId;
        $this->client_site_id = $clientSiteId;
        $this->proposal_id = $proposalId ?: NULL;
        $this->scheduled_at = $scheduleDate;
        $this->jobNo = $jobNo;
        return $this;
    }

    /**
     * Associate Job Types with this Job
     * Also removes all the older job typ associations
     *
     * @param array $jobTypeArr            
     * @return App/Http/Models/Job
     */
    public function setJobType($jobTypeArr, $removeNAdd = true)
    {
        if (is_array($jobTypeArr) && count($jobTypeArr) > 0) {
            
            if ($removeNAdd)
                (new JobType())->removeAllByJobID($this->id);
            
            foreach ($jobTypeArr as $jobType) {
                (new JobType())->addByJobID($this->id, $jobType);
            }
        }
        return $this;
    }

    /**
     * Create New JobCode using the jobNo of associated Job
     *
     * @return App/Http/Models/Jobcode
     */
    public function addNewJobcode()
    {
        return (new Jobcode())->createNewJobcode($this->jobNo, Config::get('constant.jobcode.shortcode.jobspecific'), Config::get('constant.jobcode.type.jobspecific'), Config::get('constant.option.true') );
    }

    /**
     * Allocate Resource to this particular job
     *
     * @param Array $jobResourcesData            
     */
    public function addNote($staffId, $action, $note)
    {
        return (new JobNote())->saveNote($this->id, $staffId, $action, $note);
    }

    /**
     * Add New Sign Off for given Job
     *
     * @param Array $jobResourcesData            
     */
    public function addJobSignoff($staffId, $signOffArr)
    {   
        return (new JobSignoff())->setAssociationData($this->id, $staffId)->fillnSave($signOffArr);
    }

    /**
    *  Verifies if given Vehicle is Already being assigned to Current Job
    * 
    *  @param  number  $vehicleId
    *  @return boolean
    */
    public function isVehicleAssigned($vehicleId){
        $vehicleJobResObj = JobResource::getAssignedResource($this->id, NULL,$vehicleId, Config::get('constant.resource.model.vehicle'));
        
        return ($vehicleJobResObj instanceof JobResource)? true:false;
    }

    /**
     * Allocate Staff Resource to this particular job
     *
     * @param number $staff_id Id of staff who is creating this screen
     * @return  App/Http/Models/JobResource
     */
    public function assignStaff($resourceId)
    {
        $jobResourceObj = (new JobResource())->getByJobResourceID($this->id, $resourceId, Config::get('constant.resource.model.staff'));
        
        if (! $jobResourceObj) {
            $jobResourceObj = (new JobResource())->assignResource($this->id, $resourceId, $resourceId, Config::get('constant.resource.model.staff'), 1);
        }
        return $jobResourceObj;
    }

    /**
     * Allocate Resource to this particular job
     *
     * @param Array $jobResourcesData            
     * @param Bool $deleteNAddResources            
     * @return App/Http/Models/Job
     */
    public function allocatedResources($jobResourcesData, $deleteNAddResources = true)
    {
        return JobResource::saveAllocatedResources($this->id, $jobResourcesData, $deleteNAddResources);
    }

    /**
     * Allocate Workorder to this particular job
     *
     * @param Array $jobResourcesData            
     * @return App/Http/Models/Job
     */
    public function allocateWorkOrders($jobWorkOrderData)
    {
        return (new JobWorkOrder())->saveJobWorkOrder($this->id, $jobWorkOrderData);
    }

    /**
     * Get Job Count for a given Client
     *
     * @param number $clientId            
     * @return number
     */
    public static function getJobCountByClientId($clientId)
    {
        return self::where('client_id', '=', $clientId)->count();
    }

    /**
    *  Get Job Listing By Status
    *  Status can single status string or array of status strings
    *
    *  @param  string | array $jobStatus
    */
    public static function getListByStatus($jobStatus = 'pending'){

        if( is_array($jobStatus) )
            $jobListingObj = self::whereIn('status', $jobStatus);
        else
            $jobListingObj = self::where('status' , '=', $jobStatus);

        return $jobListingObj->get();
    }

    /**
     * Update status of Job
     *
     * @param string $status            
     * @return App/Http/Models/Job Object
     */
    public function updateStatus($status)
    {
        return $this->fillnSave([
            'status' => $status
        ]);
    }

    /**
     * Get Paginated List of Job
     *
     * @param string $search            
     * @param string $filter            
     * @param number $currentPage            
     * @param number $perPage            
     * @return Collection |NULL App/Http/Models/Job Object
     */
    public function getPaginatedList($search, $filter, $currentPage, $perPage)
    {
        return $this->with('client', 'clientSite')
            ->where(function ($query) use ($search) {
            if ($search) {
                $query->orWhere('title', 'like', "%$search%");
                $query->orWhere('jobNo', 'like', "%$search%");
            }
        })
            ->where(function ($query) use ($filter) {
            if ($filter)
                $query->where('status', '=', $filter);
        })
            ->paginate($perPage, array(
            '*'
        ), 'page', $currentPage);
    }

   /**
     * Get Staff's Status for given Job
     *
     * @param number $jobId            
     * @param number $staffId            
     * @param Bool $status            
     * @return Bool
     */
    public static function getStaffJobCurrentStatus($jobId, $staffId, $status)
    {
        return (JobNote::isJobCompletionNoteAdded($jobId, $staffId)) ? Config::get('constant.status.completed') : $status;
    }

    /**
     * Get Job list for a given Client
     *
     * @param number $clientId            
     */
    public static function getJobByClientId($clientId)
    {
        return self::where('client_id', '=', $clientId)->where('status', '=', \Config::get('constant.status.completed'))
            ->select('title', 'id')
            ->get();
    }

}
