<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('workOrderNo', 45)->nullable()->default(NULL);
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('client_site_id')->unsigned();
            $table->bigInteger('proposal_id')->unsigned()->nullable()->default(NULL);
            $table->string('title', 255);
            $table->enum('type', ['compliance','construction','environmental','filtering','oilcleanup','tankremoval','vacjob']);
            $table->text('description');
            $table->text('specialInstruction');
            $table->bigInteger('followup_job_id')->unsigned()->default(0);
            $table->timestamp('scheduled_at')->nullable()->default(NULL);
            $table->enum('status', ['draft','pending','inprogress', 'completed', 'cancelled'])->default('pending');
            $table->nullableTimestamps();
            $table->softDeletes();
			
        });

        Schema::table('job', function(Blueprint $table){
            $table->foreign('client_id')->references('id')->on('client')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('client_site_id')->references('id')->on('client_site')->onDelete('cascade')->onUpdate('no action');
            $table->foreign('proposal_id')->references('id')->on('proposal')->onDelete('cascade')->onUpdate('no action');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'job' , function(Blueprint $table){
            $table->dropForeign('job_client_id_foreign');
            $table->dropForeign('job_client_site_id_foreign');
            $table->dropForeign('job_proposal_id_foreign');
        });

        Schema::dropIfExists('job');
    }
}
