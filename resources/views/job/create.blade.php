@extends('layouts.app_admin')

@section('content')
<div class="create-staff from-styling job-create-page">
		<header class="section-header header">
				<div class="tbl">
						<div class="tbl-row">
								<div class="tbl-cell">
										<h2>Job</h2>
										<div class="subtitle">Create New Job</div>
								</div>
								<div class="tbl-cell tbl-cell-action button"> <a href="{{route('portal.job.select.source')}}" class="btn btn-rounded btn-block">Select Different Source</a> </div>
						</div>
				</div>
		</header>
		@include('partials.templates.success_message')
		@include('partials.templates.server_error_message')
		<section class=""> 
			{!! Form::open(array('route' => array('portal.job.store'), 'id' => 'form-job-create', 'method' => 'post', 'class' => 'form-box', 'files' => true)) !!}
				<div class="customer-infowrap">
						<div class="row">
							<div class="col-sm-4">
									<div class="customer-info">
											<div class=""> <span class="title"> Customer Info</span> <br>
													<br>
													<div class="text-left"> <span class="title"> {{ $clientSiteObj->client->name }} </span> <span class="subtitle"> {{ ($clientSiteObj->client->isCustomer()? 'Customer': 'Prospect') }} ID # {{ $clientSiteObj->client->id }} </span> <span class="subtitle"><i class="fa icon fa-envelope-o"></i> {{ $clientSiteObj->client->email }}</span> <span class="subtitle"><i class="fa icon fa-phone"></i> {{ $clientSiteObj->client->phone }}</span> <span class="subtitle"><i class="glyphicon icon glyphicon-map-marker pull-left"></i> <span class="pull-left">{{$clientSiteObj->client->address}} <br>
															{{$clientSiteObj->client->city}}, {{$clientSiteObj->client->state->name}}  {{$clientSiteObj->client->zipcode}} <br>
															{{$clientSiteObj->client->country->name}} </span> </span> </div>
											</div>
									</div>
							</div>
							<div class="col-sm-4">
									<div class="customer-info">
											<div class=""> <span class="title"> Customer Site Info</span> <br>
													<br>
													<div class="text-left"> <span class="title"> {{ $clientSiteObj->title }} </span> <span class="subtitle">Site ID # {{ $clientSiteObj->id }} </span> <span class="subtitle"><i class="fa icon fa-user"></i> {{ $clientSiteObj->contactPerson}}</span> <br>
															<span class="subtitle"><i class="fa icon fa-phone"></i> {{ $clientSiteObj->contactNumber}}</span> <br>
															<span class="subtitle"><i class="glyphicon icon glyphicon-map-marker pull-left"></i> <span class="pull-left">{{$clientSiteObj->address}} <br>
															{{$clientSiteObj->city}}, {{$clientSiteObj->state->name}}  {{$clientSiteObj->zipcode}} <br>
															{{$clientSiteObj->country->name}} </span> </span> </div>
											</div>
									</div>
							</div>

							@if($proposalObj)
							<div class="col-sm-4">
									<div class="customer-info">
											<div class=""> <span class="title">Proposals Info</span> <br>
													<br>
													<div class="text-left"> <span class="title"> {{ $proposalObj->title }} </span> <br>
															<br>
															<span class="text-left block" > </span> <span class="subtitle"> <span class=" label status {{ $proposalObj->status }}"> {{ ucfirst($proposalObj->status) }} </span> </span> <br>
															<span class="subtitle text-center block"> <a href="{{ route('portal.proposal.edit' , $proposalObj->id) }}" class="btn btn-rounded proposal-btn" title="" target="_blank">Go to Proposal</a> </span> </div>

											</div>
									</div>
							</div>
							@endif 
						</div>
				</div>
				<?php //dump(old()); dump($errors->all()); ?>
				<div class="box-typical">
						<header class="formbox-header">
								<h2><i class="glyphicon glyphicon-info-sign"></i>Job Info</h2>
						</header>
						<div class="same-box"> 
							<!--row 1-->
							<div class="row"> 
									<!-- Input Field Title -->
									<div class="col-sm-12">
											<fieldset class="form-group {{ $errors->has('title')? 'error':'' }}">
													{!! Form::label('title', 'Title', ['class' => 'form-label semibold'] ) !!}
													{!! Form::text('title', old('title'), ['class'=>"form-control " . ($errors->has('title')? 'error':'') , 'placeholder'=>'Job Title', 'data-validation' => '[NOTEMPTY]', 'data-validation-label'=>'Job title' ]) !!}
													@if($errors->has('title')) <small class="error-list" data-error-list="">{{$errors->first('title')}}</small> @endif
											</fieldset>
									</div>
									<!-- /Input Field Title --> 
							</div>
							
							<div class="row">
								<div class="col-sm-12">
									<fieldset class="form-group {{ $errors->has('jobType')? 'error':'' }}">
										{!! Form::label('jobType', 'Job Type', ['class' => 'form-label semibold'] ) !!}
										<input type="hidden" name="jobtype-list" data-validation="[NOTEMPTY]" data-validation-label="Job type" />
										<select class="multi-select select2 form-control" name="jobType[]" multiple>

											<option value="compliance"    {{ ( old('jobType')? (in_array( 'compliance',    old('jobType'))? 'selected="selected"':'') : '' ) }}> Compliance</option>
											<option value="construction"  {{ ( old('jobType')? (in_array( 'construction',  old('jobType'))? 'selected="selected"':'') : '' ) }}> Construction</option>
											<option value="environmental" {{ ( old('jobType')? (in_array( 'environmental', old('jobType'))? 'selected="selected"':'') : '' ) }}> Environmental</option>
											<option value="filtering"     {{ ( old('jobType')? (in_array( 'filtering',     old('jobType'))? 'selected="selected"':'') : '' ) }}> Filtering</option>
											<option value="oilcleanup"    {{ ( old('jobType')? (in_array( 'oilcleanup',    old('jobType'))? 'selected="selected"':'') : '' ) }}> Oil Clean Up</option>
											<option value="tankremoval"   {{ ( old('jobType')? (in_array( 'tankremoval',   old('jobType'))? 'selected="selected"':'') : '' ) }}> Tank Removal </option>
											<option value="vacjob"        {{ ( old('jobType')? (in_array( 'vacjob',        old('jobType'))? 'selected="selected"':'') : '' ) }}> Vaccum Job </option>

										</select>
										@if($errors->has('jobType')) <small class="error-list" data-error-list="">{{$errors->first('jobType')}}</small> @endif
										
									</fieldset>
								</div>
							</div>

							<div class="row"> 
									<div class="col-sm-6">
											<fieldset class="form-group {{ $errors->has('priority')? 'error':'' }}">
												<label class="form-label semibold" for="priority">Job Priority</label>
												<!-- Input Field Status -->
												<div class=" status-box">
														<div class="row">
																<div class="col-md-12 col-sm-12">
																		<div class="checkbox-detailed checkbox-low">
																				<input type="radio" id="priority_low" name="priority" value="low" {!! (! old('priority') || old('priority') == 'low')? 'checked':'' !!}>
																				<label for="priority_low">
																					<span class="checkbox-detailed-tbl">
																						<span class="checkbox-detailed-cell">
																							<span class="checkbox-detailed-title">Low</span>
																						</span> 
																					</span>
																				</label>
																		</div>
																		<div class="checkbox-detailed checkbox-medium">
																				<input type="radio" id="priority_medium" name="priority" value="medium" {!! old('priority') == 'medium' ? 'checked':'' !!}>
																				<label for="priority_medium">
																					<span class="checkbox-detailed-tbl">
																						<span class="checkbox-detailed-cell">
																							<span class="checkbox-detailed-title">Medium</span> 
																						</span>
																					</span> 
																				</label>
																		</div>
																		<div class="checkbox-detailed checkbox-high">
																			<input type="radio" id="priority_high" name="priority" value="high" {!! old('priority') == 'high' ? 'checked':'' !!}>
																			<label for="priority_high"> 
																				<span class="checkbox-detailed-tbl">
																					<span class="checkbox-detailed-cell"> 
																					 	<span class="checkbox-detailed-title">High</span>
																					</span>
																				</span> 
																			</label>
																		</div>
																</div>
														</div>
												</div>
												@if($errors->has('priority')) <small class="error-list" data-error-list="">{{$errors->first('priority')}}</small> @endif
											</fieldset>
									</div>

									
									<div class="col-sm-6">
											<fieldset class="form-group {{ $errors->has('signoffRequired')? 'error':'' }}">
												<label class="form-label semibold" for="signoffRequired">Job Signoff Required</label>
												<!-- Input Field Status -->
												<div class=" status-box">
														<div class="row">
																<div class="col-md-12 col-sm-12">
																		<div class="checkbox-detailed">
																				<input type="radio" id="signoff-required-yes" name="signoffRequired" value="true" {!! ( old('signoffRequired') == 'low')? 'checked':'' !!}>
																				<label for="signoff-required-yes">
																					<span class="checkbox-detailed-tbl">
																						<span class="checkbox-detailed-cell">
																							<span class="checkbox-detailed-title">Yes</span>
																						</span> 
																					</span>
																				</label>
																		</div>
																		<div class="checkbox-detailed">
																				<input type="radio" id="signoff-required-no" name="signoffRequired" value="false" {!! (! old('signoffRequired') || old('signoffRequired') == 'false') ? 'checked':'' !!}>
																				<label for="signoff-required-no">
																					<span class="checkbox-detailed-tbl">
																						<span class="checkbox-detailed-cell">
																							<span class="checkbox-detailed-title">No</span> 
																						</span>
																					</span> 
																				</label>
																		</div>
																</div>
														</div>
												</div>
												@if($errors->has('signoffRequired')) <small class="error-list" data-error-list="">{{$errors->first('signoffRequired')}}</small> @endif
											</fieldset>
									</div>
							</div>

							<!--row 2-->
							<div class="row">
									<div class="col-sm-4">
											<fieldset class="form-group {{ $errors->has('jobNo')? 'error':'' }}">
													{!! Form::label('jobNo', 'Job No.', ['class' => 'form-label semibold'] ) !!}
													{!! Form::text('jobNo', old('jobNo')?:$jobNo, ['class'=>"form-control " . ($errors->has('jobNo')? 'error':'') , 'placeholder'=>'', 'data-validation' => '[NOTEMPTY, MIXED]', 'readonly' => '' ]) !!}
													
													@if($errors->has('jobNo')) <small class="error-list" data-error-list="">{{$errors->first('jobNo')}}</small> @endif
											</fieldset>
									</div>
									<div class="col-sm-4">
											<fieldset class="form-group {{ $errors->has('purchaseOrderNo')? 'error':'' }}">
													{!! Form::label('purchaseOrderNo', 'Purchase Order No.', ['class' => 'form-label semibold'] ) !!}
													{!! Form::text('purchaseOrderNo', old('purchaseOrderNo')?:$purchaseOrderNo, ['class'=>"form-control " . ($errors->has('purchaseOrderNo')? 'error':'') , 'placeholder'=>'Purchase Order No.', 'data-validation' => '[]']) !!}
													
													@if($errors->has('purchaseOrderNo')) <small class="error-list" data-error-list="">{{$errors->first('purchaseOrderNo')}}</small> @endif
											</fieldset>
									</div>
									<div class="col-md-4">
											<fieldset class="form-group datepicker-wrap {{ $errors->has('scheduleDate')? 'error':'' }}">
													{!! Form::label('scheduleDate', 'Schedule Date', ['class' => 'form-label semibold'] ) !!}
													<div class='input-group date datetimepicker-job' id="job-dtp">
															<div class="input-group-inner"> {!! Form::text('scheduleDate', '', ['class'=>'form-control', 'placeholder'=> '--/--/----' , 'data-validation' => '[NOTEMPTY, DATE]', 'data-validation-label' => 'Schedule date'] ) !!} <span class="input-group-addon"> <i class="font-icon font-icon-calend"></i> </span> </div>
													</div>
													@if($errors->has('scheduleDate')) <small class="error-list" data-error-list="">{{$errors->first('scheduleDate')}}</small> @endif
											</fieldset>
									</div>
							</div>
							<!--row 4-->
							<div class="row"> 
									<!-- Textarea Field Description -->
									<div class="col-sm-12">
											<fieldset class="form-group {{ $errors->has('description')? 'error':'' }}">
													{!! Form::label('description', 'Description', ['class' => 'form-label semibold'] ) !!}
													{!! Form::textarea('description', old('description'), ['class'=>"form-control " . ($errors->has('description')? 'error':'') , 'placeholder'=>'Job Description', 'data-validation' => '[NOTEMPTY]', 'rows' => 4, 'data-autosize', 'data-validation-label' => 'Description'] ) !!}
													@if($errors->has('description')) <small class="error-list" data-error-list="">{{$errors->first('description')}}</small> @endif
											</fieldset>
									</div>
									<!-- /Textare Field Description --> 
							</div>
							<!--row 5-->
							
							<div class="row"> 
									<!-- Textarea Field Description -->
									<div class="col-sm-12">
											<fieldset class="form-group {{ $errors->has('specialInstruction')? 'error':'' }}">
													{!! Form::label('specialInstruction', 'Special Instructions', ['class' => 'form-label semibold'] ) !!}
													{!! Form::textarea('specialInstruction', old('specialInstruction'), ['class'=>"form-control " . ($errors->has('specialInstruction')? 'error':'') , 'placeholder'=>'Special Instruction for Job', 'data-validation' => '[]', 'rows' => 4, 'data-autosize' => '', 'data-validation-label' => 'Special instructions'] ) !!}
													@if($errors->has('specialInstruction')) <small class="error-list" data-error-list="">{{$errors->first('specialInstruction')}}</small> @endif
											</fieldset>
									</div>
									<!-- /Textare Field Description --> 
							</div>
						</div>
				</div>

				<div class="box-typical job-accordion">
						<header class="formbox-header">
								<h2><i class="glyphicon glyphicon-list-alt"></i>Work Order</h2>
								<a href="#." id="add-work-order" class="btn btn-rounded btn-primary pull-right add-form-item" data-to="workorder">Add</a>
						</header>
						<div class="same-box">
								<div id="deleted-workorder-form-items" class="hide"></div>

								<div class="job-form-list-wrapper">
										<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="panel-group" id="workorder-list">
																<div class="row">
																		<p class="text-center"> <i class="fa fa-circle-o-notch fa-spin"></i> </p>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>
				
				<div class="box-typical job-accordion">
						<header class="formbox-header">
								<h2><i class="glyphicon glyphicon-list-alt"></i>PMI Manifest</h2>
								<a href="#." id="add-pmi-manifest" class="btn btn-rounded btn-primary pull-right add-form-item" data-to="pmimanifest">Add</a>
						</header>
						<div class="same-box">
								<div id="deleted-pmimanifest-form-items" class="hide"></div>

								<div class="job-form-list-wrapper">
										<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="panel-group" id="pmimanifest-list">
																<div class="row">
																		<p class="text-center"> <i class="fa fa-circle-o-notch fa-spin"></i> </p>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>

				<div class="box-typical job-accordion">
						<header class="formbox-header">
								<h2><i class="glyphicon glyphicon-list-alt"></i>Fuel Filtering Manifest</h2>
								<a href="#." id="add-fuel-manifest" class="btn btn-rounded btn-primary pull-right add-form-item" data-to="fuelmanifest">Add</a> 
						</header>
						<div class="same-box">
								<div id="deleted-fuelmanifest-form-items" class="hide"></div>

								<div class="job-form-list-wrapper">
										<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="panel-group" id="fuelmanifest-list">
																<div class="row">
																		<p class="text-center"> <i class="fa fa-circle-o-notch fa-spin"></i> </p>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>

				<div class="box-typical job-accordion">
						<header class="formbox-header">
								<h2><i class="glyphicon glyphicon-list-alt"></i>Description Of Work</h2>
								<a href="#." id="add-descript-work" class="btn btn-rounded btn-primary pull-right add-form-item" data-to="descriptwork">Add</a> 
						</header>
						<div class="same-box">
								<div id="deleted-descriptwork-form-items" class="hide"></div>

								<div class="job-form-list-wrapper">
										<div class="row">
												<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
														<div class="panel-group" id="descriptwork-list">
																<div class="row">
																		<p class="text-center"> <i class="fa fa-circle-o-notch fa-spin"></i> </p>
																</div>
														</div>
												</div>
										</div>
								</div>
						</div>
				</div>

				<div class="box-typical job-accordion">
						<header class="formbox-header">
							<h2><i class="glyphicon glyphicon-file"></i>Job Attachment</h2>
							<div class="attach-file form-group pull-right">
									<div class="btn btn-inline btn-rounded file" type="button">
									<input type="file"  id="select-attachment" data-attachment-id="{{ time() }}" accept="jpg,png,bmp,svg,gif,pdf,doc,docx" />
									<span><i class="fa fa-plus"></i> &nbsp; Add Attachment</span>
									<br>
									</div>
							</div>
						</header>
						<div class="same-box">
							<div class="job-form-list-wrapper">
								<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="proposal-attachments" id="job-attachment-list" name="job-attachment-list">
													<div class="attachment no-item">
														<p class="">No attachments has been attached to this job</p>
													</div>
												</div>
										</div>
								</div>	
							</div>
						</div>
				</div>


				<div class="box-typical">
						<header class="formbox-header">
								<h2><i class="fa fa-user"></i> Managers</h2>
						</header>
						<div class="same-box">
								<div class="row"> 
										
										<!-- Input Field Job Type -->
										<div class="col-sm-12">
												<fieldset class="form-group {{ $errors->has('manager')? 'error':'' }}">
														{!! Form::label('manager', 'Assign Managers', ['class' => 'form-label semibold'] ) !!}
														<select class="multi-select select2-photo-limited form-control" name="manager[]" multiple data-msg-select-limit="Only %s managers can be assigned to a job" >
															@foreach($managerListObj as $managerObj)
																<option value="{{ $managerObj->id}}"  data-photo="{{ asset(Common::getUserProfileImage( $managerObj->profileImage)) }}" {{ ( old('manager')? (in_array($managerObj->id, old('manager'))? 'selected="selected"':'') : '' ) }}> {{ Common::getFullName($managerObj) }} </option>
															@endforeach
														</select>
														@if($errors->has('manager')) <small class="error-list" data-error-list="">{{$errors->first('manager')}}</small> @endif
												</fieldset>
										</div>
										<!-- /Input Field Job Type --> 
								</div>
						</div>
						<header class="formbox-header">
								<h2><i class="fa fa-user"></i> Technicians</h2>
						</header>
						<div class="same-box">
								<div class="row"> 
										
										<!-- Input Field Job Type -->
										<div class="col-sm-12">
												<fieldset class="form-group {{ $errors->has('staff')? 'error':'' }}">
														{!! Form::label('staff', 'Assign Technicians', ['class' => 'form-label semibold'] ) !!}
														<select class="multi-select select2-photo form-control" name="staff[]" multiple>
																
															@foreach($staffListObj as $staffObj)
									
																<option value="{{ $staffObj->id}}"  data-photo="{{ asset(Common::getUserProfileImage( $staffObj->profileImage)) }}" {{ ( old('staff')? (in_array($staffObj->id, old('staff'))? 'selected="selected"':'') : '' ) }}> 
																{{ Common::getFullName($staffObj) }} </option>
																
															@endforeach
							
														</select>
														@if($errors->has('staff')) <small class="error-list" data-error-list="">{{$errors->first('staff')}}</small> @endif
												</fieldset>
										</div>
										<!-- /Input Field Job Type --> 
								</div>
						</div>
						<header class="formbox-header">
								<h2><i class="fa fa-truck"></i>Vehicles</h2>
						</header>
						<div class="same-box">
								<div class="row"> 
										<!-- Input Field Job Type -->
										<div class="col-sm-12">
												<fieldset class="form-group {{ $errors->has('vehicle')? 'error':'' }}">
														{!! Form::label('vehicle', 'Assign Vehicles', ['class' => 'form-label semibold'] ) !!}
														<select class="multi-select select2-photo form-control" name="vehicle[]" multiple>
																
															@foreach($vehicleListObj as $vehicleObj)
										
																<option value="{{ $vehicleObj->id }}" data-photo="{{ asset(Common::getIconByVehicleType($vehicleObj->type))}}" data-icon="fa fa-truck" {{ ( old('vehicle')? (in_array($vehicleObj->id, old('vehicle'))? 'selected="selected"':'') : '' )}}> {{ $vehicleObj->label}}</option>
																
															@endforeach
								
														</select>
														@if($errors->has('vehicle')) <small class="error-list" data-error-list="">{{$errors->first('vehicle')}}</small> @endif
												</fieldset>
										</div>
										<!-- /Input Field Job Type --> 
								</div>
						</div>
						<header class="formbox-header">
								<h2><i class="font-icon font-icon font-icon-notebook-bird "></i>Inventory</h2>
						</header>
						<div class="same-box"> 
								<!--row 1-->
								<div class="row">
										<div class="add-inv-item-wrapper col-md-12"> {!! Form::label('inventoryItem', 'Allocate Inventory Items', ['class' => 'form-label semibold'] ) !!}
												<div class="row"> 
														<!-- Input Field Job Type -->
														<div class="col-sm-6">
																<fieldset class="form-group {{ $errors->has('inventoryItem')? 'error':'' }}">
																		<select class="select2" id="inventory-item-select">
																				
																			@foreach($inventoryItemListObj as $inventoryItemObj)
												
																				<option value="{{ $inventoryItemObj->id}}" data-max="{{ $inventoryItemObj->total }}" data-label="{{ $inventoryItemObj->label }}">{{ $inventoryItemObj->label }}</option>
																				
																			@endforeach
										
																		</select>
																		@if($errors->has('inventoryItem')) <small class="error-list">{{$errors->first('inventoryItem')}}</small> @endif
																</fieldset>
														</div>
														<!-- /Input Field Job Type -->
														
														<div class="col-sm-4 col-md-4 col-lg-4 quantity-box {{ $errors->has('quantity')? 'error':'' }}">
																<fieldset class="form-group">
																		{!! Form::text('', '', ['class'=>"text-center" . ($errors->has('quantity')? 'error':'') , 'placeholder'=>'Quantity', 'data-validation-label'=>'Item quantity' , 'id' => 'item-quantity']) !!}
																		
																		@if($errors->has('quantity')) <small class="error-list" data-error-list="">{{$errors->first('quantity')}}</small> @endif
																</fieldset>
																
																<!-- /Input Field Job Type --> 
														</div>
														<div class="col-sm-2">
																<div class="">
																		<button type="button" id="add-inv-item" class="col-md-12 btn btn-rounded btn-inline btn-primary add">Add Item</button>
																</div>
														</div>
												</div>
										</div>
 
								</div>
								<!--row 1-->
								<div class="inventory-items inventory-box">
										<div class="row" id="inventory-list">
												<p class="text-center"> <i class="fa fa-circle-o-notch fa-spin"></i> </p>
										</div>
								</div>
								 
								
								
						</div>
						<div class="same-box button-box">
								<div class="row"> 
										<!-- Input Field Change Password and Cancel Buttons -->
										<div class="col-sm-12">
												<div class="form-group"> {!! Form::submit('Create', array('class' => 'btn btn-rounded btn-inline btn-primary pull-right') ) !!} <a href="{{route('portal.job.listing')}}" class="btn btn-rounded btn-inline btn-secondary">Cancel</a> </div>
										</div>
								</div>
						</div>
				</div>
				<!--End Rows--> 
				{!! Form::close() !!} </section>
</div>


<script type="text/javascript" src="{{ asset('assets/js/lib/common/underscore-min.js')}}"></script> 

<script type="text/javascript" src="{{ asset('assets/js/lib/jquery-tag-editor/jquery.caret.min.js')}}"></script> 
<script type="text/javascript" src="{{ asset('assets/js/lib/jquery-tag-editor/jquery.tag-editor.min.js')}}"></script> 
<script type="text/javascript" src="{{ asset('assets/js/app/job-attachment.js')}}" ></script> 

<script type="text/javascript">
	
	var JobForms = { WorkOrder : 'workorder', PMIManifest : 'pmimanifest', FuelManifest : 'fuelmanifest', DescriptionOfWork : 'descriptwork'};

	var isServerError  = {!! old()? 'true':'false' !!} 
	var inventoryItems = {!! json_encode($selectedItemsArr) !!};
	var workOrderItems = {!! json_encode( old('workorder')? $oldWorkOrderItemsArr: [] ) !!};
	var pmiManifestItems = {!! json_encode( old('pmimanifest')? $oldPmiManifestItemsArr: [] ) !!};
	var fuelManifestItems = {!! json_encode( old('fuelmanifest')? $oldFuelManifestItemsArr: [] ) !!};
	var descriptWorkItems = {!! json_encode( old('descriptwork')? $oldDescriptWorkItemsArr: [] ) !!};
	
	console.log(isServerError);

</script> 

@include('job.templates.frontend.workorder-list-item') 
@include('job.templates.frontend.pmimanifest-list-item') 
@include('job.templates.frontend.fuelmanifest-list-item')
@include('job.templates.frontend.descriptwork-list-item')
@include('job.templates.frontend.attachment')

<script type="text/javascript" src="{{ asset('assets/js/app/job-form-operation.js')}}"></script> 

@include('job.templates.frontend.inventory-list-item') 
<script type="text/javascript" src="{{ asset('assets/js/app/job-inventory-item-feature.js')}}"></script> 

<script type="text/javascript">
	jQuery(document).ready(function($){
		itemlist.init();
		attachmentlist.init({});
		jobFormList.init( JobForms.WorkOrder, workOrderItems, isServerError);
		jobFormList.init( JobForms.PMIManifest, pmiManifestItems, isServerError);
		jobFormList.init( JobForms.FuelManifest, fuelManifestItems, isServerError);
		jobFormList.init( JobForms.DescriptionOfWork, descriptWorkItems, isServerError);

		$('body').on('click', 'a[data-toggle="collapse"]',function(){
			var objectID=$(this).attr('href');
			$(objectID).collapse( ($(objectID).hasClass('in')? 'hide' : 'show') );
		});

		$('.select2[name="jobType[]"]').on('change', function(e){
			console.log('Changed', e , $(this).val());
			var selectedValues = $(this).val();
			var implodedValues = '';

			if( ! _.isUndefined(selectedValues) && !  _.isNull(selectedValues) ) {
				implodedValues = selectedValues.join();
			}

			$('[name="jobtype-list"]').val(implodedValues);

		});
	});

</script> 

@include('job.templates.frontend.jobform-download-route')

@stop